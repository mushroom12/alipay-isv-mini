# 支付宝小程序模板开发，一整套流程

https://mp.weixin.qq.com/s/35SGSC6WGQwYCWZ9tlOIVw

![Image text](https://pic1.zhimg.com/v2-146c19a8f22ef71c16873d3ba2251f25_1440w.jpg?source=172ae18b)

#### 介绍
支付宝小程序服务商模板开发着力于解决支付宝生态体系内的小程序管理问题，一套模板，随处部署。能尽可能地减少系统服务商的开发成本，系统服务商只用开发一套小程序代码作为模板就可以快速批量的孵化出大量的商家小程序。

![Image text](https://pic3.zhimg.com/80/v2-4a70a198af349daacefc43e6f960965a_720w.png)

比如餐饮类的小程序，大部分功能都相同，这时候服务商只要通过模板开发，得到商家授权后，即可快速根据模板代码给商家快速实例化小程序。无需关心各个商家的小程序资料信息，繁琐操作那些开发配置信息。多个前端只需对应一个服务端后台。时间少，成本低。

#### 业务特点：

开发流程：服务商先开发小程序模板的代码，然后通过小程序模板的代码去构建商家小程序的代码。

快速开通：通过服务市场，商家仅需完成订购，即完成商家小程序授权，通过模板快速构建商家小程序版本。

批量维护：服务商可代商家实现小程序的快速批量更新。

#### 1、添加商户基本信息

![Image text](https://pic1.zhimg.com/80/v2-080c20e990a442ffaf5741452e605bec_720w.png)

#### 2、引导商户授权获取授权信息

第三方服务商构建授权链接生成二维码放置自己的网站，用户点击后，弹出二维码，管理员打开支付宝APP扫描授权。

![Image text](https://pic2.zhimg.com/80/v2-9ce7c6fa6eae0c27518942a9e86789b5_720w.png)

#### 3、修改小程序基本资料

在这里可以直接修改小程序资料，而无需跑到各个小程序支付宝后台去修改。包括小程序名称、小程序英文名、主营行业、小程序简介、小程序描述、小程序logo、小程序描述、客服电话、客服邮箱。

![Image text](https://pic2.zhimg.com/80/v2-b552dc9d5d86e600be039bbc0b9c91e9_720w.png)

![Image text](https://pic2.zhimg.com/80/v2-8fe15bf23e639ac873a44be39fcee07d_720w.png)

#### 4、版本管理

整个系统的核心就在这个模块，包括版本上传、提交审核、删除版本、撤销审核、退回开发、设为体验版、取消体验版、上架版本、下架版本、回滚版本等。

![Image text](https://pic3.zhimg.com/80/v2-d3620c8c4c404bee7ad1e598d3acb6a6_720w.png)

#### 4.1、版本上传

小程序基于模板上传版本

![Image text](https://pic3.zhimg.com/80/v2-f8a36824e05f2c572d34fb6986f4bf86_720w.png)

#### 4.2、提交审核

![Image text](https://pic3.zhimg.com/80/v2-37a2a6adf232ffd3ec9da5ffb2496146_720w.png)

![Image text](https://pic2.zhimg.com/80/v2-4281e74315507c4fea113b0180b72011_720w.png)

#### 4.3、设为体验版

![Image text](https://pic3.zhimg.com/80/v2-1630234d84200112a5dad834c64d6e7e_720w.png)

#### 4.4、查看审核失败原因

![Image text](https://pic3.zhimg.com/80/v2-17ee726663d0c981c4da748b021d59d6_720w.png)

#### 5、成员管理

服务商在帮助旗下授权的小程序提交代码审核之前，可先让小程序运营者体验，体验之前需要将运营者的个人支付宝账号添加到该小程序的体验者名单中。 可以添加开发者和体验者。

![Image text](https://pic3.zhimg.com/80/v2-3815a5a41129c5b4cf0bfaed58c44912_720w.png)

![Image text](https://pic3.zhimg.com/80/v2-726918066c69aa80f19038b361dc9256_720w.png)

#### 6、码管理

码管理包括小程序和关联普通二维码模块。

![Image text](https://pic1.zhimg.com/80/v2-d1a5f225441e96168fd3e34fcbc48c4c_720w.png)

#### 6.1、小程序码管理

商家小程序上架后，在这里可以生成任意已有路径的小程序码进行推广。

![Image text](https://pic4.zhimg.com/80/v2-eff89408c7c2ccbc7c3344e517ca25af_720w.png)

#### 6.2、关联普通二维码管理

商户可不需更换线下二维码，通过该接口完成配置后，用户用支付宝扫描普通二维码时打开小程序并跳转到指定页面，普通二维码关联小程序最多可设置20条规则。

![Image text](https://pic4.zhimg.com/80/v2-cf5be89af7e17c432975c5445d95fa07_720w.png)

体验

公众号后台回复【支付宝模板开发】获取账号密码。

![Image text](https://pic1.zhimg.com/80/v2-fc64ca6384d51bffb28eb6e100c1185c_720w.png)

山水有相逢，来日皆可期，谢谢阅读，我们再会

我手中的金箍棒，上能通天，下能探海
